package tsc.abzalov.tm.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.enumeration.Role;

import java.util.UUID;

import static tsc.abzalov.tm.constant.LiteralConst.DEFAULT_LAST_NAME;
import static tsc.abzalov.tm.enumeration.Role.USER;

public final class User {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String login;

    @NotNull
    private String hashedPassword;

    @NotNull
    private Role role = USER;

    @NotNull
    private String firstName;

    @Nullable
    private String lastName;

    @NotNull
    private String email;

    public User(
            @NotNull String login, @NotNull String hashedPassword,
            @NotNull Role role, @NotNull String firstName,
            @Nullable String lastName, @NotNull String email
    ) {
        this.login = login;
        this.hashedPassword = hashedPassword;
        this.role = role;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public User(
            @NotNull String login, @NotNull String hashedPassword,
            @NotNull Role role, @NotNull String firstName,
            @NotNull String email
    ) {
        this.login = login;
        this.hashedPassword = hashedPassword;
        this.role = role;
        this.firstName = firstName;
        this.email = email;
    }

    public User(
            @NotNull String login, @NotNull String hashedPassword,
            @NotNull String firstName, @Nullable String lastName,
            @NotNull String email
    ) {
        this.login = login;
        this.hashedPassword = hashedPassword;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public User(
            @NotNull String login, @NotNull String hashedPassword,
            @NotNull String firstName, @NotNull String email
    ) {
        this.login = login;
        this.hashedPassword = hashedPassword;
        this.firstName = firstName;
        this.email = email;
    }

    @NotNull
    public String getId() {
        return id;
    }

    public void setId(@NotNull String id) {
        this.id = id;
    }

    @NotNull
    public String getLogin() {
        return login;
    }

    public void setLogin(@NotNull String login) {
        this.login = login;
    }

    @NotNull
    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(@NotNull String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    @NotNull
    public Role getRole() {
        return role;
    }

    public void setRole(@NotNull Role role) {
        this.role = role;
    }

    @NotNull
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(@NotNull String firstName) {
        this.firstName = firstName;
    }

    @Nullable
    public String getLastName() {
        return lastName;
    }

    public void setLastName(@Nullable String lastName) {
        this.lastName = lastName;
    }

    @NotNull
    public String getEmail() {
        return email;
    }

    public void setEmail(@NotNull String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        final String correctLastName = (this.lastName == null) ? DEFAULT_LAST_NAME : this.lastName;
        return "[ID: " + this.id +
                "; Login: " + this.login +
                "; Role: " + this.role.getDisplayName() +
                "; First Name: " + this.firstName +
                "; Last Name: " + correctLastName +
                "; Email: " + this.email + "]";
    }

}
