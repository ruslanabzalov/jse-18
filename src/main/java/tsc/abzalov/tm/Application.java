package tsc.abzalov.tm;

import tsc.abzalov.tm.bootstrap.CommandBootstrap;

public final class Application {

    public static void main(String... args) {
        final CommandBootstrap commandBootstrap = new CommandBootstrap();
        commandBootstrap.run(args);
    }

}
