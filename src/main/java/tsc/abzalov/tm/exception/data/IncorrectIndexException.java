package tsc.abzalov.tm.exception.data;

import tsc.abzalov.tm.exception.AbstractException;

public final class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException(int index) {
        super("Index \"" + index + "\" is incorrect!");
    }

}
