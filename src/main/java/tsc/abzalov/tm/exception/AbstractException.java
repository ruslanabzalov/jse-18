package tsc.abzalov.tm.exception;

public abstract class AbstractException extends Exception {

    public AbstractException(String message) {
        super("\n" + message + "\n");
    }

}
