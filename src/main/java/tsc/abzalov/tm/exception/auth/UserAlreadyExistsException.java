package tsc.abzalov.tm.exception.auth;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.exception.AbstractException;

public final class UserAlreadyExistsException extends AbstractException {

    public UserAlreadyExistsException(@NotNull String login, @NotNull String email) {
        super("User with login '" + login + "' or email '" + email + "' is already exist." +
                "Please, register user with another login.");
    }

}
