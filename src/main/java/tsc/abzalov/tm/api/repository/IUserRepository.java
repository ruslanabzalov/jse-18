package tsc.abzalov.tm.api.repository;

import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.model.User;

public interface IUserRepository {

    void create(String login, String hashedPassword, Role role, String firstName, String lastName, String email);

    void create(String login, String hashedPassword, String firstName, String lastName, String email);

    User findUserByLogin(String login);

    User findUserByEmail(String email);

    User updateUserByLogin(String login, String hashedPassword);

}
