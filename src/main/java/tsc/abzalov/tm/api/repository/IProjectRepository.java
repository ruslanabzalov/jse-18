package tsc.abzalov.tm.api.repository;

import tsc.abzalov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    int size(String userId);

    int indexOf(Project project, String userId);

    void createProject(String id, String description, String userId);

    List<Project> findAllProjects(String userId);

    Project findProjectById(String id, String userId);

    Project findProjectByIndex(int index, String userId);

    Project findProjectByName(String name, String userId);

    Project updateProjectById(String id, String name, String description, String userId);

    Project updateProjectByIndex(int index, String name, String description, String userId);

    Project updateProjectByName(String name, String description, String userId);

    void deleteAllProjects(String userId);

    void deleteProjectById(String id, String userId);

    void deleteProjectByIndex(int index, String userId);

    void deleteProjectByName(String name, String userId);

    Project startProjectById(String id, String userId);

    Project endProjectById(String id, String userId);

}
