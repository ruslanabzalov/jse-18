package tsc.abzalov.tm.api.repository;

import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    int size(String userId);

    int indexOf(Task task, String userId);

    void createTask(String id, String description, String userId);

    void addTaskToProjectById(String projectId, String taskId, String userId);

    List<Task> findAllTasks(String userId);

    Task findTaskById(String id, String userId);

    Task findTaskByIndex(int index, String userId);

    Task findTaskByName(String name, String userId);

    List<Task> findProjectTasksById(String projectId, String userId);

    Task updateTaskById(String id, String name, String description, String userId);

    Task updateTaskByIndex(int index, String name, String description, String userId);

    Task updateTaskByName(String name, String description, String userId);

    void deleteAllTasks(String userId);

    void deleteTaskById(String id, String userId);

    void deleteTaskByIndex(int index, String userId);

    void deleteTaskByName(String name, String userId);

    void deleteProjectTasksById(String taskId, String userId);

    void deleteProjectTaskById(String taskId, String userId);

    Task startTaskById(String id, String userId);

    Task endTaskById(String id, String userId);
    
}
