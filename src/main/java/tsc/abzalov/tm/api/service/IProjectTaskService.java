package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    int indexOf(Task task, String userId);

    boolean hasData(String userId);

    void addTaskToProjectById(String projectId, String taskId, String userId) throws Exception;

    Project findProjectById(String id, String userId) throws Exception;

    Task findTaskById(String id, String userId) throws Exception;

    List<Task> findProjectTasksById(String projectId, String userId) throws Exception;

    void deleteProjectById(String id, String userId) throws Exception;

    void deleteProjectTasksById(String projectId, String userId) throws Exception;

    void deleteProjectTaskById(String taskId, String userId) throws Exception;

}
