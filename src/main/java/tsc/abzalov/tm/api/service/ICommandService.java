package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.exception.general.IncorrectCommandException;

import java.util.Collection;

public interface ICommandService {

    Collection<AbstractCommand> getCommands();

    Collection<String> getCommandNames();

    Collection<String> getCommandArguments();

    AbstractCommand getCommandByName(String name) throws IncorrectCommandException;

    AbstractCommand getArgumentByName(String name) throws IncorrectCommandException;

    void add(AbstractCommand command);

}
