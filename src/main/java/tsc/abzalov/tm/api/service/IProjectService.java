package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    int size(String userId);

    int indexOf(Project project, String userId);

    void createProject(String name, String description, String userId) throws Exception;

    List<Project> findAllProjects(String userId);

    Project findProjectById(String id, String userId) throws Exception;

    Project findProjectByIndex(int index, String userId) throws Exception;

    Project findProjectByName(String name, String userId) throws Exception;

    Project updateProjectById(String id, String name, String description, String userId) throws Exception;

    Project updateProjectByIndex(int index, String name, String description, String userId) throws Exception;

    Project updateProjectByName(String name, String description, String userId) throws Exception;

    void deleteAllProjects(String userId);

    void deleteProjectById(String id, String userId) throws Exception;

    void deleteProjectByIndex(int index, String userId) throws Exception;

    void deleteProjectByName(String name, String userId) throws Exception;

    Project startProjectById(String id, String userId) throws Exception;

    Project endProjectById(String id, String userId) throws Exception;

    List<Project> sortProjectsByName(String userId);

    List<Project> sortProjectsByStartDate(String userId);

    List<Project> sortProjectsByEndDate(String userId);

    List<Project> sortProjectsByStatus(String userId);
    
}
