package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.model.Task;

import java.util.List;

public interface ITaskService {

    int size(String userId);

    int indexOf(Task task, String userId);

    void createTask(String name, String description, String userId) throws Exception;

    List<Task> findAllTasks(String userId);

    Task findTaskById(String id, String userId) throws Exception;

    Task findTaskByIndex(int index, String userId) throws Exception;

    Task findTaskByName(String name, String userId) throws Exception;

    Task updateTaskById(String id, String name, String description, String userId) throws Exception;

    Task updateTaskByIndex(int index, String name, String description, String userId) throws Exception;

    Task updateTaskByName(String name, String description, String userId) throws Exception;

    void deleteAllTasks(String userId);

    void deleteTaskById(String id, String userId) throws Exception;

    void deleteTaskByIndex(int index, String userId) throws Exception;

    void deleteTaskByName(String name, String userId) throws Exception;

    Task startTaskById(String id, String userId) throws Exception;

    Task endTaskById(String id, String userId) throws Exception;

    List<Task> sortTasksByName(String userId);

    List<Task> sortTasksByStartDate(String userId);

    List<Task> sortTasksByEndDate(String userId);

    List<Task> sortTasksByStatus(String userId);
    
}
