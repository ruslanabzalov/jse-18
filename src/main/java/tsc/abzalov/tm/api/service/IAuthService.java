package tsc.abzalov.tm.api.service;

public interface IAuthService {

    void register(
            String login, String password,
            String firstName, String lastName,
            String email
    ) throws Exception;

    void login(String login, String password) throws Exception;

    void logoff() throws Exception;

    boolean isSessionInactive();

    String getCurrentUserLogin() throws Exception;

    String getCurrentUserId() throws Exception;

}
