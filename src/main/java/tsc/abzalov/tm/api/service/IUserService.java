package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.model.User;

public interface IUserService {

    void create(
            String login, String password,
            Role role, String firstName,
            String lastName, String email
    ) throws Exception;

    void create(
            String login, String password,
            String firstName, String lastName,
            String email
    ) throws Exception;

    User findUserByLogin(String login) throws Exception;

    User findUserByEmail(String email) throws Exception;

    User updateUserByLogin(String login, String password) throws Exception;

}
