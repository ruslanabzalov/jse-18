package tsc.abzalov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.api.service.*;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.command.auth.AuthChangePasswordCommand;
import tsc.abzalov.tm.command.auth.AuthLoginCommand;
import tsc.abzalov.tm.command.auth.AuthLogoffCommand;
import tsc.abzalov.tm.command.auth.AuthRegisterCommand;
import tsc.abzalov.tm.command.interaction.CommandAddTaskToProject;
import tsc.abzalov.tm.command.interaction.CommandDeleteProjectTask;
import tsc.abzalov.tm.command.interaction.CommandDeleteProjectTasks;
import tsc.abzalov.tm.command.interaction.CommandShowProjectTasks;
import tsc.abzalov.tm.command.project.*;
import tsc.abzalov.tm.command.sorting.*;
import tsc.abzalov.tm.command.system.*;
import tsc.abzalov.tm.command.task.*;
import tsc.abzalov.tm.command.user.UserShowInfoCommand;
import tsc.abzalov.tm.repository.CommandRepository;
import tsc.abzalov.tm.repository.ProjectRepository;
import tsc.abzalov.tm.repository.TaskRepository;
import tsc.abzalov.tm.repository.UserRepository;
import tsc.abzalov.tm.service.*;

import java.util.Arrays;
import java.util.List;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static tsc.abzalov.tm.enumeration.Role.ADMIN;
import static tsc.abzalov.tm.util.InputUtil.INPUT;

public final class CommandBootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IUserRepository userRepository = new UserRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ILoggerService loggerService = new LoggerService();

    public CommandBootstrap() {
        initCommand(new SystemAboutCommand());
        initCommand(new SystemArgumentsCommand());
        initCommand(new SystemCommandsCommand());
        initCommand(new SystemExitCommand());
        initCommand(new SystemHelpCommand());
        initCommand(new SystemInfoCommand());
        initCommand(new SystemVersionCommand());
        initCommand(new ProjectCreateCommand());
        initCommand(new ProjectDeleteAllCommand());
        initCommand(new ProjectDeleteByIdCommand());
        initCommand(new ProjectDeleteByIndexCommand());
        initCommand(new ProjectDeleteByNameCommand());
        initCommand(new ProjectEndByIdCommand());
        initCommand(new ProjectShowAllCommand());
        initCommand(new ProjectShowByIdCommand());
        initCommand(new ProjectShowByIndexCommand());
        initCommand(new ProjectShowByNameCommand());
        initCommand(new ProjectStartByIdCommand());
        initCommand(new ProjectUpdateByIdCommand());
        initCommand(new ProjectUpdateByIndexCommand());
        initCommand(new ProjectUpdateByNameCommand());
        initCommand(new TaskCreateCommand());
        initCommand(new TaskDeleteAllCommand());
        initCommand(new TaskDeleteByIdCommand());
        initCommand(new TaskDeleteByIndexCommand());
        initCommand(new TaskDeleteByNameCommand());
        initCommand(new TaskEndByIdCommand());
        initCommand(new TaskShowAllCommand());
        initCommand(new TaskShowByIdCommand());
        initCommand(new TaskShowByIndexCommand());
        initCommand(new TaskShowByNameCommand());
        initCommand(new TaskStartByIdCommand());
        initCommand(new TaskUpdateByIdCommand());
        initCommand(new TaskUpdateByIndexCommand());
        initCommand(new TaskUpdateByNameCommand());
        initCommand(new CommandAddTaskToProject());
        initCommand(new CommandDeleteProjectTask());
        initCommand(new CommandDeleteProjectTasks());
        initCommand(new CommandShowProjectTasks());
        initCommand(new SortingProjectsByEndDateCommand());
        initCommand(new SortingProjectsByNameCommand());
        initCommand(new SortingProjectsByStartDateCommand());
        initCommand(new SortingProjectsByStatusCommand());
        initCommand(new SortingTasksByEndDateCommand());
        initCommand(new SortingTasksByNameCommand());
        initCommand(new SortingTasksByStartDateCommand());
        initCommand(new SortingTasksByStatusCommand());
        initCommand(new AuthRegisterCommand());
        initCommand(new AuthLoginCommand());
        initCommand(new AuthLogoffCommand());
        initCommand(new UserShowInfoCommand());
        initCommand(new AuthChangePasswordCommand());
    }

    {
        try {
            userService.create("admin", "admin", ADMIN, "Admin", "", "admin@mail.com");
            userService.create("test", "test", "Test", "", "test@mail.com");
        } catch (Exception exception) {
            loggerService.error(exception);
        }
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    public void run(@NotNull String... args) {
        System.out.println();
        try {
            if (areArgExists(args)) return;
        } catch (Exception exception) {
            loggerService.error(exception);
            return;
        }

        loggerService.info("****** WELCOME TO TASK MANAGER APPLICATION ******");
        System.out.println("Please, use \"help\" command to see all available commands.\n");
        System.out.println("Please, login/register new user to start work with the application.\n");

        final List<String> availableStartupCommands = Arrays.asList("register", "login", "logoff", "help", "exit");

        String commandName;
        //noinspection InfiniteLoopStatement
        while (true) {
            System.out.print("Please, enter your command: ");
            commandName = INPUT.nextLine();
            System.out.println();
            if (isEmpty(commandName)) continue;

            if (authService.isSessionInactive()) {
                if (!availableStartupCommands.contains(commandName)) {
                    System.out.println("Session is inactive! Please, register new user or login.");
                    continue;
                }
            }

            try {
                loggerService.command(commandName);
                commandService.getCommandByName(commandName).execute();
            }
            catch (Exception exception) {
                loggerService.error(exception);
            }
        }
    }

    private void initCommand(@NotNull AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private boolean areArgExists(@NotNull String... args) throws Exception {
        if (isEmpty(args)) return false;

        final String arg = args[0];
        if (isEmpty(arg)) return false;

        commandService.getArgumentByName(arg).execute();
        return true;
    }

}
