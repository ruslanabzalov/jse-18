package tsc.abzalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository implements IUserRepository {

    private final List<User> users = new ArrayList<>();

    @Override
    public void create(
            @NotNull String login, @NotNull String hashedPassword,
            @NotNull Role role, @NotNull String firstName,
            @Nullable String lastName, @NotNull String email
    ) {
        users.add(new User(login, hashedPassword, role, firstName, lastName, email));
    }

    @Override
    public void create(
            @NotNull String login, @NotNull String hashedPassword,
            @NotNull String firstName, @Nullable String lastName,
            @NotNull String email
    ) {
        users.add(new User(login, hashedPassword, firstName, lastName, email));
    }

    @Override
    @Nullable
    public User findUserByLogin(@NotNull String login) {
        return users.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User findUserByEmail(String email) {
        return users.stream()
                .filter(user -> email.equals(user.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public User updateUserByLogin(String login, String hashedPassword) {
        return users.stream()
                .filter(user -> login.equals(user.getLogin()))
                .findFirst()
                .map(user -> {
                    user.setHashedPassword(hashedPassword);
                    return user;
                })
                .orElse(null);
    }

}
