package tsc.abzalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.model.Task;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static tsc.abzalov.tm.enumeration.Status.*;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public int size(@NotNull String userId) {
        return (int) tasks.stream()
                .filter(task -> userId.equals(task.getUserId()))
                .count();
    }

    @Override
    public int indexOf(@NotNull final Task task, @NotNull String userId) {
        return tasks.stream()
                .filter(searchedTask -> userId.equals(searchedTask.getUserId()))
                .collect(Collectors.toList())
                .indexOf(task);
    }

    @Override
    public void createTask(
            @NotNull final String id, @NotNull final String description,
            @NotNull String userId
    ) {
        tasks.add(new Task(id, description, userId));
    }

    @Override
    public void addTaskToProjectById(
            @NotNull final String projectId, @NotNull final String taskId,
            @NotNull String userId
    ) {
        tasks.stream()
                .filter(task -> userId.equals(task.getUserId()) && taskId.equals(task.getId()))
                .findFirst()
                .map(task -> {
                    task.setProjectId(projectId);
                    return task;
                });
    }

    @Override
    @NotNull
    public List<Task> findAllTasks(@NotNull String userId) {
        return tasks.stream()
                .filter(task -> userId.equals(task.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    public Task findTaskById(@NotNull final String id, @NotNull String userId) {
        return tasks.stream()
                .filter(task -> userId.equals(task.getUserId()) && id.equals(task.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public Task findTaskByIndex(final int index, @NotNull String userId) {
        return tasks.stream()
                .filter(task -> userId.equals(task.getUserId()))
                .collect(Collectors.toList())
                .get(index);
    }

    @Override
    @Nullable
    public Task findTaskByName(@NotNull final String name, @NotNull String userId) {
        return tasks.stream()
                .filter(task -> userId.equals(task.getUserId()) && name.equals(task.getName()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @NotNull
    public List<Task> findProjectTasksById(@NotNull final String taskId, @NotNull String userId) {
        return tasks.stream()
                .filter(task -> isTaskTaskExist(taskId, task))
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    public Task updateTaskById(
            @NotNull final String id, @NotNull final String name,
            @NotNull final String description, @NotNull String userId
    ) {
        final Task task = findTaskById(id, userId);
        if (task == null) return null;

        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    @Nullable
    public Task updateTaskByIndex(
            final int index, @NotNull final String name,
            @NotNull final String description, @NotNull String userId
    ) {
        final Task task = findTaskByIndex(index, userId);
        if (task == null) return null;

        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    @Nullable
    public Task updateTaskByName(
            @NotNull final String name, @NotNull final String description,
            @NotNull String userId
    ) {
        final Task task = findTaskByName(name, userId);
        if (task == null) return null;

        task.setDescription(description);
        return task;
    }

    @Override
    public void deleteAllTasks(@NotNull String userId) {
        tasks.removeIf(task -> userId.equals(task.getUserId()));
    }

    @Override
    public void deleteTaskById(@NotNull final String id, @NotNull String userId) {
        final Task task = findTaskById(id, userId);
        if (task == null) return;

        tasks.remove(task);
    }

    @Override
    public void deleteTaskByIndex(final int index, @NotNull String userId) {
        final Task task = findTaskByIndex(index, userId);
        if (task == null) return;

        tasks.remove(task);
    }

    @Override
    public void deleteTaskByName(@NotNull final String name, @NotNull String userId) {
        final Task task = findTaskByName(name, userId);
        if (task == null) return;

        tasks.remove(task);
    }

    @Override
    public void deleteProjectTasksById(@NotNull final String taskId, @NotNull final String userId) {
        tasks.removeAll(findProjectTasksById(taskId, userId));
    }

    @Override
    public void deleteProjectTaskById(@NotNull final String taskId, @NotNull final String userId) {
        for (final Task task : tasks) {
            if (task.getId().equals(taskId)) {
                if (task.getProjectId() != null) {
                    task.setProjectId(null);
                }
            }
        }
    }

    @Override
    public Task startTaskById(@NotNull final String id, @NotNull String userId) {
        final Task task = findTaskById(id, userId);
        if (task == null || task.getStatus() != TODO) return null;

        task.setStatus(IN_PROGRESS);
        task.setStartDate(LocalDateTime.now());
        return task;
    }

    @Override
    public Task endTaskById(@NotNull final String id, @NotNull String userId) {
        final Task task = findTaskById(id, userId);
        if (task == null || task.getStatus() != IN_PROGRESS) return null;

        task.setStatus(DONE);
        task.setEndDate(LocalDateTime.now());
        return task;
    }

    private boolean isTaskTaskExist(
            @NotNull final String taskId, @NotNull final Task task
    ) {
        final String projectId = task.getProjectId();
        if (projectId == null) return false;

        return projectId.equals(taskId);
    }
    
}
