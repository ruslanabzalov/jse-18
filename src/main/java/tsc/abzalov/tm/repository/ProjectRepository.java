package tsc.abzalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.model.Project;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static tsc.abzalov.tm.enumeration.Status.*;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public int size(@NotNull String userId) {
        return (int) projects.stream()
                .filter(project -> userId.equals(project.getUserId()))
                .count();
    }

    @Override
    public int indexOf(@NotNull final Project project, @NotNull String userId) {
        return projects.stream()
                .filter(searchedProject -> userId.equals(searchedProject.getUserId()))
                .collect(Collectors.toList())
                .indexOf(project);
    }

    @Override
    public void createProject(
            @NotNull final String id, @NotNull final String description,
            @NotNull String userId
    ) {
        projects.add(new Project(id, description, userId));
    }

    @Override
    @NotNull
    public List<Project> findAllProjects(@NotNull String userId) {
        return projects.stream()
                .filter(project -> userId.equals(project.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    @Nullable
    public Project findProjectById(@NotNull final String id, @NotNull String userId) {
        return projects.stream()
                .filter(project -> userId.equals(project.getUserId()) && id.equals(project.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public Project findProjectByIndex(final int index, @NotNull String userId) {
        return projects.stream()
                .filter(project -> userId.equals(project.getUserId()))
                .collect(Collectors.toList())
                .get(index);
    }

    @Override
    @Nullable
    public Project findProjectByName(@NotNull final String name, @NotNull String userId) {
        return projects.stream()
                .filter(project -> userId.equals(project.getUserId()) && name.equals(project.getName()))
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public Project updateProjectById(
            @NotNull final String id, @NotNull final String name,
            @NotNull final String description, @NotNull String userId
    ) {
        final Project project = findProjectById(id, userId);
        if (project == null) return null;

        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    @Nullable
    public Project updateProjectByIndex(
            final int index, @NotNull final String name,
            @NotNull final String description, @NotNull String userId
    ) {
        final Project project = findProjectByIndex(index, userId);
        if (project == null) return null;

        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    @Nullable
    public Project updateProjectByName(
            @NotNull final String name, @NotNull final String description,
            @NotNull String userId
    ) {
        final Project project = findProjectByName(name, userId);
        if (project == null) return null;

        project.setDescription(description);
        return project;
    }

    @Override
    public void deleteAllProjects(@NotNull String userId) {
        projects.removeIf(project -> userId.equals(project.getUserId()));
    }

    @Override
    public void deleteProjectById(@NotNull final String id, @NotNull String userId) {
        final Project project = findProjectById(id, userId);
        if (project == null) return;

        projects.remove(project);
    }

    @Override
    public void deleteProjectByIndex(final int index, @NotNull String userId) {
        final Project project = findProjectByIndex(index, userId);
        if (project == null) return;

        projects.remove(project);
    }

    @Override
    public void deleteProjectByName(@NotNull final String name, @NotNull String userId) {
        final Project project = findProjectByName(name, userId);
        if (project == null) return;

        projects.remove(project);
    }

    @Override
    public Project startProjectById(@NotNull final String id, @NotNull String userId) {
        final Project project = findProjectById(id, userId);
        if (project == null || project.getStatus() != TODO) return null;

        project.setStatus(IN_PROGRESS);
        project.setStartDate(LocalDateTime.now());
        return project;
    }

    @Override
    public Project endProjectById(@NotNull final String id, @NotNull String userId) {
        final Project project = findProjectById(id, userId);
        if (project == null || project.getStatus() != IN_PROGRESS) return null;

        project.setStatus(DONE);
        project.setEndDate(LocalDateTime.now());
        return project;
    }

}
