package tsc.abzalov.tm.command.project;

import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;

public final class ProjectDeleteAllCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "delete-all-projects";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Delete all projects.";
    }

    @Override
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("PROJECTS DELETION\n");
        final IProjectService projectService = serviceLocator.getProjectService();
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        final boolean areProjectsExist = projectService.size(currentUserId) != 0;
        if (areProjectsExist) {
            projectService.deleteAllProjects(currentUserId);
            System.out.println("Projects were deleted.\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

}
