package tsc.abzalov.tm.command.project;

import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.PROJECT_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputDescription;
import static tsc.abzalov.tm.util.InputUtil.inputName;

public final class ProjectCreateCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "create-project";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Create project.";
    }

    @Override
    public CommandType getCommandType() {
        return PROJECT_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("PROJECT CREATION\n");
        final String projectName = inputName();
        final String projectDescription = inputDescription();
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();

        serviceLocator.getProjectService().createProject(projectName, projectDescription, currentUserId);

        System.out.println("Project was successfully created.\n");
    }

}
