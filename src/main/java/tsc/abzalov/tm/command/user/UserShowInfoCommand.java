package tsc.abzalov.tm.command.user;

import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.USER_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputLogin;

public class UserShowInfoCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "show-user-info";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show all user information.";
    }

    @Override
    public CommandType getCommandType() {
        return USER_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("SHOW USER INFO");
        final String userLogin = inputLogin();

        System.out.println(serviceLocator.getUserService().findUserByLogin(userLogin) + "\n");
    }

}
