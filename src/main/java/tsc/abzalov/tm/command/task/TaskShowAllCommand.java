package tsc.abzalov.tm.command.task;

import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.enumeration.CommandType;

import java.util.List;

import static tsc.abzalov.tm.enumeration.CommandType.TASK_COMMAND;

public final class TaskShowAllCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "show-all-tasks";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public CommandType getCommandType() {
        return TASK_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ALL TASKS LIST\n");
        final ITaskService taskService = serviceLocator.getTaskService();
        final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        final boolean areTasksExist = taskService.size(currentUserId) != 0;
        if (areTasksExist) {
            final List<Task> tasks = taskService.findAllTasks(currentUserId);
            tasks.forEach(task -> System.out.println((taskService.indexOf(task, currentUserId) + 1) + ". " + task));
            System.out.println();
            return;
        }

        System.out.println("Tasks list is empty.\n");
    }

}
