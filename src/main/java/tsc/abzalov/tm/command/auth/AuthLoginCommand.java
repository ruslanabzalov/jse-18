package tsc.abzalov.tm.command.auth;

import tsc.abzalov.tm.api.service.IAuthService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.AUTH_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.*;

public final class AuthLoginCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "login";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Login as existing user.";
    }

    @Override
    public CommandType getCommandType() {
        return AUTH_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        final IAuthService authService = serviceLocator.getAuthService();

        System.out.println("LOGIN\n");

        final String login = inputLogin();
        final String password = inputPassword();

        authService.login(login, password);

        System.out.println("Successful login.");
    }

}
