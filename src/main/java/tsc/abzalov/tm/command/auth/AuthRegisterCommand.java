package tsc.abzalov.tm.command.auth;

import tsc.abzalov.tm.api.service.IAuthService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.AUTH_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.*;

public final class AuthRegisterCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "register";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Register new user.";
    }

    @Override
    public CommandType getCommandType() {
        return AUTH_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        final IAuthService authService = serviceLocator.getAuthService();

        System.out.println("CREATE USER\n");

        final String login = inputLogin();
        final String password = inputPassword();
        final String firstName = inputFirstName();
        final String lastName = inputLastName();
        final String email = inputEmail();

        authService.register(login, password, firstName, lastName, email);

        System.out.println("User was successfully registered.");
    }

}
