package tsc.abzalov.tm.command.auth;

import tsc.abzalov.tm.api.service.IAuthService;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;

import static tsc.abzalov.tm.enumeration.CommandType.AUTH_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.*;

public final class AuthLogoffCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "logoff";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Logoff from existing user.";
    }

    @Override
    public CommandType getCommandType() {
        return AUTH_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        final IAuthService authService = serviceLocator.getAuthService();

        System.out.println("LOGOFF\n");

        authService.logoff();

        System.out.println("Successful logoff.");
    }

}
