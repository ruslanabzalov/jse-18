package tsc.abzalov.tm.command.auth;

import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.auth.UserIsNotExistException;
import tsc.abzalov.tm.model.User;

import static tsc.abzalov.tm.enumeration.CommandType.AUTH_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputPassword;

public class AuthChangePasswordCommand extends AbstractCommand {

    @Override
    public String getCommandName() {
        return "change-password";
    }

    @Override
    public String getCommandArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Change current user password.";
    }

    @Override
    public CommandType getCommandType() {
        return AUTH_COMMAND;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("PASSWORD CHANGING");
        final String newPassword = inputPassword();
        final String currentUserLogin = serviceLocator.getAuthService().getCurrentUserLogin();
        final User updatedUser = serviceLocator.getUserService().updateUserByLogin(currentUserLogin, newPassword);

        if (updatedUser == null) throw new UserIsNotExistException(currentUserLogin);

        System.out.println("Password successfully changed.");
    }

}
