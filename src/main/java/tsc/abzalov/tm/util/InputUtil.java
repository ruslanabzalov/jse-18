package tsc.abzalov.tm.util;

import org.jetbrains.annotations.NotNull;

import java.io.Console;
import java.util.Scanner;

import static org.apache.commons.lang3.StringUtils.isBlank;

public final class InputUtil {

    public static final Scanner INPUT = new Scanner(System.in);

    public static final Console CONSOLE = System.console();

//    public static final String EMAIL_REGEX ="(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*" +
//            "|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]" +
//            "|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)" +
//            "+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.)" +
//            "{3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:" +
//            "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])" +
//            "+)\\])\n";

    public static boolean isPositiveIntDigit(@NotNull String someString) {
        final boolean isDigit = someString.matches("^\\d+$");
        return isDigit && Integer.parseInt(someString) > 0;
    }

    public static boolean isCorrectIndex(int index, int listSize) {
        return index >= 0 && index <= listSize - 1;
    }

    @NotNull
    public static String inputId() {
        System.out.print("Please, enter an ID: ");
        String projectId = INPUT.nextLine();
        while (isBlank(projectId)) {
            System.out.print("ID cannot be empty! Please, enter an correct ID: ");
            projectId = INPUT.nextLine();
        }
        return projectId;
    }

    public static int inputIndex() {
        System.out.print("Please, enter an index: ");
        String projectIndex = INPUT.nextLine();
        boolean isPositiveIntDigit = isPositiveIntDigit(projectIndex);
        while (!isPositiveIntDigit) {
            System.out.print("Incorrect index! Please, enter correct one: ");
            projectIndex = INPUT.nextLine();
            isPositiveIntDigit = isPositiveIntDigit(projectIndex);
        }
        return Integer.parseInt(projectIndex);
    }

    @NotNull
    public static String inputName() {
        System.out.print("Please, enter a name: ");
        String projectName = INPUT.nextLine();
        while (isBlank(projectName)) {
            System.out.print("Name cannot be empty! Please, enter correct name: ");
            projectName = INPUT.nextLine();
        }
        return projectName;
    }

    public static String inputDescription() {
        System.out.print("Please, enter a description: ");
        return INPUT.nextLine();
    }

    @NotNull
    public static String inputLogin() {
        System.out.print("Please, enter a login: ");
        String login = INPUT.nextLine();
        while (isBlank(login)) {
            System.out.print("Login cannot be empty! Please, enter correct login: ");
            login = INPUT.nextLine();
        }
        return login;
    }

    @NotNull
    public static String inputPassword() {
        System.out.print("Please, enter a password: ");
        String password = INPUT.nextLine();
        while (isBlank(password)) {
            System.out.print("Password cannot be empty! Please, enter correct password: ");
            password = INPUT.nextLine();
        }
        return password;
    }

    @NotNull
    public static String inputFirstName() {
        System.out.print("Please, enter first name: ");
        String firstName = INPUT.nextLine();
        while (isBlank(firstName)) {
            System.out.print("First name cannot be empty! Please, enter correct first name: ");
            firstName = INPUT.nextLine();
        }
        return firstName;
    }

    public static String inputLastName() {
        System.out.print("Please, enter a last name: ");
        return INPUT.nextLine();
    }

    @NotNull
    public static String inputEmail() {
        System.out.print("Please, enter an email: ");
        String email = INPUT.nextLine();
        while (isBlank(email)) {
            System.out.print("Email cannot be empty! Please, enter correct email: ");
            email = INPUT.nextLine();
        }
        return email;
    }

    private InputUtil() {
    }

}
