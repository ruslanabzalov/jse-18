package tsc.abzalov.tm.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static java.security.MessageDigest.getInstance;

public final class HashUtil {

    private static final String ALGORITHM = "MD5";

    private static final int COUNTER = 128;

    private static final String SALT = "fwjojf23f2-323";

    public static String hash(String password) throws Exception {
        if (password == null) return null;

        String hashedPassword = password;
        for (int i = 0; i < COUNTER; i++) hashedPassword = md5(SALT + hashedPassword + SALT);
        return hashedPassword;
    }

    private static String md5(String hash) throws Exception {
        if (hash == null) return null;

        try {
            final MessageDigest md = getInstance(ALGORITHM);
            byte[] bytes = md.digest(hash.getBytes());
            StringBuilder builder = new StringBuilder();
            for (byte b : bytes) builder.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);

            return builder.toString();
        } catch (NoSuchAlgorithmException exception) {
            throw new Exception(exception);
        }
    }

    private HashUtil() {
    }
}
