package tsc.abzalov.tm.service;

import org.apache.commons.lang3.ObjectUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IAuthService;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.exception.auth.*;
import tsc.abzalov.tm.model.User;

import static org.apache.commons.lang3.StringUtils.isAnyBlank;
import static tsc.abzalov.tm.util.HashUtil.hash;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String currentUserLogin;

    public AuthService(@NotNull IUserService userService) {
        this.userService = userService;
    }

    @Override
    public void register(
            @NotNull String login, @NotNull String password,
            @NotNull String firstName, @NotNull String lastName,
            @NotNull String email
    ) throws Exception {
        if (ObjectUtils.anyNotNull(userService.findUserByLogin(login), userService.findUserByEmail(email)))
            throw new UserAlreadyExistsException(login, email);

        userService.create(login, password, firstName, lastName, email);
        currentUserLogin = login;
    }


    @Override
    public void login(@NotNull String login, @NotNull String password) throws Exception {
        if (isAnyBlank(login, password)) throw new IncorrectCredentialsException();
        final User user = userService.findUserByLogin(login);
        if (user == null) throw new UserIsNotExistException(login);
        if (user.getHashedPassword().equals(hash(password))) {
            currentUserLogin = user.getLogin();
            return;
        }

        throw new AccessDeniedException();
    }

    @Override
    public void logoff() throws Exception {
        if (currentUserLogin == null) throw new SessionIsInactiveException();

        currentUserLogin = null;
    }


    @Override
    public boolean isSessionInactive() {
        return currentUserLogin == null;
    }

    @Override
    @NotNull
    public String getCurrentUserLogin() throws Exception {
        if (currentUserLogin == null) throw new SessionIsInactiveException();

        return currentUserLogin;
    }

    @Override
    @NotNull
    public String getCurrentUserId() throws Exception {
        if (currentUserLogin == null) throw new SessionIsInactiveException();

        return userService.findUserByLogin(currentUserLogin).getId();
    }

}
