package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IUserRepository;
import tsc.abzalov.tm.api.service.IUserService;
import tsc.abzalov.tm.enumeration.Role;
import tsc.abzalov.tm.exception.auth.EmptyEmailException;
import tsc.abzalov.tm.exception.auth.EmptyFirstNameException;
import tsc.abzalov.tm.exception.auth.IncorrectCredentialsException;
import tsc.abzalov.tm.model.User;

import static org.apache.commons.lang3.StringUtils.isAnyBlank;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static tsc.abzalov.tm.util.HashUtil.hash;

public final class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(@NotNull IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void create(
            @NotNull String login, @NotNull String password,
            @NotNull Role role, @NotNull String firstName,
            @Nullable String lastName, @NotNull String email
    ) throws Exception {
        if (isAnyBlank(login, password)) throw new IncorrectCredentialsException();
        if (isBlank(firstName)) throw new EmptyFirstNameException();
        if (isBlank(email)) throw new EmptyEmailException();

        userRepository.create(login, hash(password), role, firstName, lastName, email);
    }

    @Override
    public void create(
            @NotNull String login, @NotNull String password,
            @NotNull String firstName, @Nullable String lastName,
            @NotNull String email
    ) throws Exception {
        if (isAnyBlank(login, password)) throw new IncorrectCredentialsException();
        if (isBlank(firstName)) throw new EmptyFirstNameException();
        if (isBlank(email)) throw new EmptyEmailException();

        userRepository.create(login, hash(password), firstName, lastName, email);
    }

    @Override
    public User findUserByLogin(@NotNull String login) throws Exception {
        if (isBlank(login)) throw new IncorrectCredentialsException();

        return userRepository.findUserByLogin(login);
    }

    @Override
    public User findUserByEmail(String email) throws Exception {
        if (isBlank(email)) throw new EmptyEmailException();

        return userRepository.findUserByEmail(email);
    }

    @Override
    public User updateUserByLogin(String login, String password) throws Exception {
        if (isAnyBlank(login, password)) throw new IncorrectCredentialsException();

        return userRepository.updateUserByLogin(login, hash(password));
    }

}
