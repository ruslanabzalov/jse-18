package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EmptyNameException;
import tsc.abzalov.tm.exception.data.IncorrectIndexException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.enumeration.Status;

import java.time.LocalDateTime;
import java.util.List;

import static org.apache.commons.lang3.ObjectUtils.anyNull;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static tsc.abzalov.tm.constant.LiteralConst.DEFAULT_DESCRIPTION;
import static tsc.abzalov.tm.util.InputUtil.isCorrectIndex;

public final class ProjectService implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public int size(@NotNull String userId) {
        return projectRepository.size(userId);
    }

    @Override
    public int indexOf(@NotNull final Project project, @NotNull String userId) {
        return projectRepository.indexOf(project, userId);
    }

    @Override
    public void createProject(
            @NotNull final String name, @Nullable String description,
            @NotNull String userId
    ) throws Exception {
        if (isBlank(name)) throw new EmptyNameException();
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;

        projectRepository.createProject(name, description, userId);
    }

    @Override
    @NotNull
    public List<Project> findAllProjects(@NotNull String userId) {
        return sortProjectsByStartDate(userId);
    }

    @Override
    @Nullable
    public Project findProjectById(@NotNull final String id, @NotNull String userId) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();

        return projectRepository.findProjectById(id, userId);
    }

    @Override
    @Nullable
    public Project findProjectByIndex(final int index, @NotNull String userId) throws Exception {
        final boolean isIndexCorrect = isCorrectIndex(index, projectRepository.size(userId));
        if (isIndexCorrect) return projectRepository.findProjectByIndex(index - 1, userId);
        throw new IncorrectIndexException(index);
    }

    @Override
    @Nullable
    public Project findProjectByName(@NotNull final String name, @NotNull String userId) throws Exception {
        if (isBlank(name)) throw new EmptyNameException();

        return projectRepository.findProjectByName(name, userId);
    }

    @Override
    @Nullable
    public Project updateProjectById(
            @NotNull final String id, @NotNull final String name,
            @Nullable String description, @NotNull String userId
    ) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();
        if (isBlank(name)) throw new EmptyNameException();
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;

        return projectRepository.updateProjectById(id, name, description, userId);
    }

    @Override
    @Nullable
    public Project updateProjectByIndex(
            final int index, @NotNull String name,
            @Nullable String description, @NotNull String userId
    )  throws Exception {
        final boolean isIndexCorrect = isCorrectIndex(index, projectRepository.size(userId));
        if (!isIndexCorrect) throw new IncorrectIndexException(index);
        if (isBlank(name)) throw new EmptyNameException();
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;

        return projectRepository.updateProjectByIndex(index - 1, name, description, userId);
    }

    @Override
    @Nullable
    public Project updateProjectByName(
            @NotNull final String name, @Nullable String description,
            @NotNull String userId
    ) throws Exception {
        if (isBlank(name)) throw new EmptyNameException();
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;

        return projectRepository.updateProjectByName(name, description, userId);
    }

    @Override
    public void deleteAllProjects(@NotNull String userId) {
        projectRepository.deleteAllProjects(userId);
    }

    @Override
    public void deleteProjectById(@NotNull final String id, @NotNull String userId) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();

        projectRepository.deleteProjectById(id, userId);
    }

    @Override
    public void deleteProjectByIndex(final int index, @NotNull String userId) throws Exception {
        final boolean isIndexCorrect = isCorrectIndex(index, projectRepository.size(userId));
        if (!isIndexCorrect) throw new IncorrectIndexException(index);

        projectRepository.deleteProjectByIndex(index - 1, userId);
    }

    @Override
    public void deleteProjectByName(@NotNull final String name, @NotNull String userId) throws Exception {
        if (isBlank(name)) throw new EmptyNameException();

        projectRepository.deleteProjectByName(name, userId);
    }

    @Override
    @Nullable
    public Project startProjectById(@NotNull final String id, @NotNull String userId) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();

        return projectRepository.startProjectById(id, userId);
    }

    @Override
    @Nullable
    public Project endProjectById(@NotNull final String id, @NotNull String userId) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();

        return projectRepository.endProjectById(id, userId);
    }

    @Override
    @NotNull
    public List<Project> sortProjectsByName(@NotNull String userId) {
        final List<Project> projects = projectRepository.findAllProjects(userId);

        projects.sort((firstProject, secondProject) -> {
            final String firstProjectName = firstProject.getName();
            final String secondProjectName = secondProject.getName();
            return firstProjectName.compareTo(secondProjectName);
        });

        return projects;
    }

    @Override
    @NotNull
    public List<Project> sortProjectsByStartDate(@NotNull String userId) {
        final List<Project> projects = projectRepository.findAllProjects(userId);

        projects.sort((firstProject, secondProject) -> {
            final LocalDateTime firstProjectStartDate = firstProject.getStartDate();
            final LocalDateTime secondProjectStartDate = secondProject.getStartDate();
            if (anyNull(firstProjectStartDate, secondProjectStartDate)) return 0;
            return firstProjectStartDate.compareTo(secondProjectStartDate);
        });

        return projects;
    }

    @Override
    @NotNull
    public List<Project> sortProjectsByEndDate(@NotNull String userId) {
        final List<Project> projects = projectRepository.findAllProjects(userId);

        projects.sort((firstProject, secondProject) -> {
            final LocalDateTime firstProjectEndDate = firstProject.getEndDate();
            final LocalDateTime secondProjectEndDate = secondProject.getEndDate();
            if (anyNull(firstProjectEndDate, secondProjectEndDate)) return 0;
            return firstProjectEndDate.compareTo(secondProjectEndDate);
        });

        return projects;
    }

    @Override
    @NotNull
    public List<Project> sortProjectsByStatus(@NotNull String userId) {
        final List<Project> projects = projectRepository.findAllProjects(userId);

        projects.sort((firstProject, secondProject) -> {
            final Status firstProjectStatus = firstProject.getStatus();
            final Status secondProjectStatus = secondProject.getStatus();
            return firstProjectStatus.compareTo(secondProjectStatus);
        });

        return projects;
    }
    
}
