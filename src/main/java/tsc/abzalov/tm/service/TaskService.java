package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.exception.data.EmptyIdException;
import tsc.abzalov.tm.exception.data.EmptyNameException;
import tsc.abzalov.tm.exception.data.IncorrectIndexException;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.enumeration.Status;

import java.time.LocalDateTime;
import java.util.List;

import static org.apache.commons.lang3.ObjectUtils.anyNull;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static tsc.abzalov.tm.constant.LiteralConst.DEFAULT_DESCRIPTION;
import static tsc.abzalov.tm.util.InputUtil.isCorrectIndex;

public final class TaskService implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public int size(@NotNull String userId) {
        return taskRepository.size(userId);
    }

    @Override
    public int indexOf(@NotNull final Task task, @NotNull String userId) {
        return taskRepository.indexOf(task, userId);
    }

    @Override
    public void createTask(
            @NotNull final String name, @Nullable String description,
            @NotNull String userId
    ) throws Exception {
        if (isBlank(name)) throw new EmptyNameException();
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;

        taskRepository.createTask(name, description, userId);
    }

    @Override
    @NotNull
    public List<Task> findAllTasks(@NotNull String userId) {
        return sortTasksByStartDate(userId);
    }

    @Override
    @Nullable
    public Task findTaskById(@NotNull final String id, @NotNull String userId) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();

        return taskRepository.findTaskById(id, userId);
    }

    @Override
    @Nullable
    public Task findTaskByIndex(final int index, @NotNull String userId) throws Exception {
        final boolean isIndexCorrect = isCorrectIndex(index, taskRepository.size(userId));
        if (isIndexCorrect) return taskRepository.findTaskByIndex(index - 1, userId);
        throw new IncorrectIndexException(index);
    }

    @Override
    @Nullable
    public Task findTaskByName(@NotNull final String name, @NotNull String userId) throws Exception {
        if (isBlank(name)) throw new EmptyNameException();

        return taskRepository.findTaskByName(name, userId);
    }

    @Override
    @Nullable
    public Task updateTaskById(
            @NotNull final String id, @NotNull final String name,
            @Nullable String description, @NotNull String userId
    ) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();
        if (isBlank(name)) throw new EmptyNameException();
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;

        return taskRepository.updateTaskById(id, name, description, userId);
    }

    @Override
    @Nullable
    public Task updateTaskByIndex(
            final int index, @NotNull String name,
            @Nullable String description, @NotNull String userId
    )  throws Exception {
        final boolean isIndexCorrect = isCorrectIndex(index, taskRepository.size(userId));
        if (!isIndexCorrect) throw new IncorrectIndexException(index);
        if (isBlank(name)) throw new EmptyNameException();
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;

        return taskRepository.updateTaskByIndex(index - 1, name, description, userId);
    }

    @Override
    @Nullable
    public Task updateTaskByName(
            @NotNull final String name, @Nullable String description,
            @NotNull String userId
    ) throws Exception {
        if (isBlank(name)) throw new EmptyNameException();
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;

        return taskRepository.updateTaskByName(name, description, userId);
    }

    @Override
    public void deleteAllTasks(@NotNull String userId) {
        taskRepository.deleteAllTasks(userId);
    }

    @Override
    public void deleteTaskById(@NotNull final String id, @NotNull String userId) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();

        taskRepository.deleteTaskById(id, userId);
    }

    @Override
    public void deleteTaskByIndex(final int index, @NotNull String userId) throws Exception {
        final boolean isIndexCorrect = isCorrectIndex(index, taskRepository.size(userId));
        if (!isIndexCorrect) throw new IncorrectIndexException(index);

        taskRepository.deleteTaskByIndex(index - 1, userId);
    }

    @Override
    public void deleteTaskByName(@NotNull final String name, @NotNull String userId) throws Exception {
        if (isBlank(name)) throw new EmptyNameException();

        taskRepository.deleteTaskByName(name, userId);
    }

    @Override
    @Nullable
    public Task startTaskById(@NotNull final String id, @NotNull String userId) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();

        return taskRepository.startTaskById(id, userId);
    }

    @Override
    @Nullable
    public Task endTaskById(@NotNull final String id, @NotNull String userId) throws Exception {
        if (isBlank(id)) throw new EmptyIdException();

        return taskRepository.endTaskById(id, userId);
    }

    @Override
    @NotNull
    public List<Task> sortTasksByName(@NotNull String userId) {
        final List<Task> tasks = taskRepository.findAllTasks(userId);

        tasks.sort((firstTask, secondTask) -> {
            final String firstTaskName = firstTask.getName();
            final String secondTaskName = secondTask.getName();
            return firstTaskName.compareTo(secondTaskName);
        });

        return tasks;
    }

    @Override
    @NotNull
    public List<Task> sortTasksByStartDate(@NotNull String userId) {
        final List<Task> tasks = taskRepository.findAllTasks(userId);

        tasks.sort((firstTask, secondTask) -> {
            final LocalDateTime firstTaskStartDate = firstTask.getStartDate();
            final LocalDateTime secondTaskStartDate = secondTask.getStartDate();
            if (anyNull(firstTaskStartDate, secondTaskStartDate)) return 0;
            return firstTaskStartDate.compareTo(secondTaskStartDate);
        });

        return tasks;
    }

    @Override
    @NotNull
    public List<Task> sortTasksByEndDate(@NotNull String userId) {
        final List<Task> tasks = taskRepository.findAllTasks(userId);

        tasks.sort((firstTask, secondTask) -> {
            final LocalDateTime firstTaskEndDate = firstTask.getEndDate();
            final LocalDateTime secondTaskEndDate = secondTask.getEndDate();
            if (anyNull(firstTaskEndDate, secondTaskEndDate)) return 0;
            return firstTaskEndDate.compareTo(secondTaskEndDate);
        });

        return tasks;
    }

    @Override
    @NotNull
    public List<Task> sortTasksByStatus(@NotNull String userId) {
        final List<Task> tasks = taskRepository.findAllTasks(userId);

        tasks.sort((firstTask, secondTask) -> {
            final Status firstTaskStatus = firstTask.getStatus();
            final Status secondTaskStatus = secondTask.getStatus();
            return firstTaskStatus.compareTo(secondTaskStatus);
        });

        return tasks;
    }

}
